var express = require('express');
var mqtt = require('mqtt');
var router = express.Router();

// user / password mqtt
var options = {
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD
}

router.post('/', function(req, res, next) {
  var reply = '';
  reply += "expediteur: " + req.body.topic;
  reply += "<br/>l1: " + req.body.ligne1;
  reply += "<br/>l2: " + req.body.ligne2;
  reply += "<br/>l3: " + req.body.ligne3;
  reply += "<br/>l4: " + req.body.ligne4;
  reply += "<br/>l5: " + req.body.ligne5;
  var message = req.body.ligne1 + ";" + req.body.ligne2 + ";" + req.body.ligne3 + ";" + req.body.ligne4 + ";" + req.body.ligne5 + ";";
  console.log(message)
  var topic = req.body.topic;
  var client = mqtt.connect(process.env.MQTT_CLOUD_URL, options);
  client.on('connect', function() {
    client.subscribe(topic, function(err) {
      if (!err) {
        client.publish(topic, message)
      }
    })
  })
  res.send(reply);
});

module.exports = router;
